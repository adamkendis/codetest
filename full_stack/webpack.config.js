const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const clientPath = path.resolve(__dirname, 'client');

module.exports = {
  entry: path.join(clientPath, 'src/index.js'),
  output: {
    path: path.resolve(clientPath, 'dist'),
    filename: 'bundle.js',
  },
  devServer: {
    contentBase: path.resolve(clientPath, 'public'),
    port: 3000,
    hot: true,
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(clientPath, 'public/index.html'),
      title: 'FlyBox',
    }),
    new Dotenv({
      path: './.env'
    }),
  ],
}
