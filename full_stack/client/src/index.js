import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));

// Enables Hot Module Replacement (HMR) in webpack
// HMR updates modules at runtime without a full app refresh
if (module.hot) {
  module.hot.accept();
}