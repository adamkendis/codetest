import axios from 'axios';

const API_PATH = '/api/flies';

const getAllFlies = async () => {
  const { data } = await axios.get(API_PATH);
  return data;
};

const createFly = async flyData => {
  const { data } = await axios.post(API_PATH, flyData);
  return data;
}

const updateFly = async flyData => {
  const { id } = flyData;
  const url = `${API_PATH}/${id}`;
  const { data } = await axios.patch(url, flyData);
  return data;
}

const deleteFly = async id => {
  const url = `${API_PATH}/${id}`;
  const { data } = await axios.delete(url);
  return data;
}

const getFlyTypes = async () => {
  const url = '/api/flytypes';
  const { data } = await axios.get(url);
  return data;
}

export {
  getAllFlies,
  createFly,
  updateFly,
  deleteFly,
  getFlyTypes,
};
