import React, { useState, useEffect } from 'react';
import {
  getAllFlies,
  getFlyTypes,
  createFly,
  updateFly,
  deleteFly,
} from './services/api';
import Header from './components/Header';
import Card, { CardActions } from './components/Card/';
import CreateCard from './components/CreateCard';
import './App.css';

const App = () => {
  const [loading, setLoading] = useState(true);
  const [creating, setCreating] = useState(false);
  const [updating, setUpdating] = useState(false);
  const [flies, setFlies] = useState(null);
  const [flyTypes, setFlyTypes] = useState(null);
  const [currIndex, setCurrIndex] = useState(0);
  
  // Fetch all flies from api and sync state on initial mount
  useEffect(() => {
    const fetchData = async () => {
      const fliesData = await(getAllFlies());
      const flyTypesData = await(getFlyTypes());
      const typesById = flyTypesData.reduce((acc, cur) => {
        acc[cur.id] = cur;
        return acc;
      }, {})
      setFlies(fliesData);
      setFlyTypes(typesById);
    }
    fetchData();
  },[]);

  // Toggle loading flag off after all data is loaded
  useEffect(() => {
    if (flies && flyTypes) {
      setLoading(false);
    }
  }, [flies, flyTypes])

  const handleNext = () => {
    if (currIndex === flies.length - 1) setCurrIndex(0);
    else setCurrIndex(prevIndex => prevIndex + 1);
  }

  const handleSubmit = async newFly => {
    const data = await(createFly(newFly));
    if (data){
      data.type = flyTypes[data.typeId];
      setCurrIndex(0);
      setFlies(prevFlies => [data, ...prevFlies]);
      if (creating) toggleCreate()
      if (updating) toggleUpdate()
    }
  }
  
  const handleUpdate = async fly => {
    const data = await(updateFly(fly));
    if (data) {
      data.type = flyTypes[data.typeId];
      flies[currIndex] = data;
      setFlies(flies);
      toggleUpdate();
    }
    return data;
  }

  const handleDelete = async flyId => {
    const { status } = await(deleteFly(flyId));
    if (status === 'success') {
      const updatedFlies =
        flies.filter(({ id }) => id !== flyId);
      setFlies(updatedFlies);
    }
  }

  // Toggles new CreateCard
  const toggleCreate = () => {
    setCreating(prevCreating => !prevCreating);
  }

  // Toggles CreateCard with prepopulated fields for editing
  const toggleUpdate = () => {
    setUpdating(prevUpdating => !prevUpdating);
  }

  if (loading) return <div className="loader">Loading...</div>

  return (
    <div className="app">
      <Header />
      <div className="card-container">
        {  
          // Render CreateCard if user is creating/updating card
          // or the stack is already empty
          (creating || updating || !flies.length) ? (
            <CreateCard
              flyTypes={flyTypes}
              handleSubmit={handleSubmit}
              handleUpdate={handleUpdate}
              creating={creating}
              card={flies[currIndex]}
            />
          ) : (
            <Card card={flies[currIndex]}>
              <CardActions
                onClickNext={handleNext}
                onClickDelete={() => handleDelete(flies[currIndex].id)}
                onClickUpdate={toggleUpdate}
                toggleCreate={toggleCreate}
              />
            </Card>
          )
        }
      </div>
    </div>
  );
};

export default App;
