import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './CreateCard.css';

const CreateCard = ({
  handleSubmit,
  handleUpdate,
  flyTypes,
  card,
  creating,
}) => {
  const initialState = creating ? {
    descr: '',
    factoid: '',
    imgUrl: '',
    typeId: 1,
  } : card
  const [newCard, setNewCard] = useState(initialState);

  const handleChange = e => {
    const { name, value } = e.target;
    setNewCard(prevNewFly => ({
      ...prevNewFly,
      [name]: value
    }));
  };

  return (
    <form
      className="card-form"
      onSubmit={(e) => {
        e.preventDefault();
        if (creating) handleSubmit(newCard);
        else handleUpdate(newCard);
      }}
    >
      <label>
        Name of fly:
        <input
          name="descr"
          type="text"
          required
          onChange={handleChange}
          value={newCard.descr}
        />
      </label>
      <label>
        Type of fly:
        <select
          name="typeId"
          onChange={handleChange}
          value={newCard.typeId}
        >
          {
            flyTypes ? Object.values(flyTypes).map(type => (
              <option
                key={type.id}
                value={type.id}
              >
                {type.name}
              </option>
            )) : 'loading...'
          }
        </select>
      </label>
      <label>
        Fly factoid:
        <textarea
          name="factoid"
          required
          onChange={handleChange}
          value={newCard.factoid}
        ></textarea>
      </label>
      <label>
        Fly image url:
        <input
          name="imgUrl"
          type="url"
          required
          onChange={handleChange}
          value={newCard.imgUrl}
        />
      </label>
      <div>
        <button
          className="button-submit"
          type="submit"
          value="Submit"
        >
          Submit
        </button>
      </div>
    </form>
  )
}

export default CreateCard;

CreateCard.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleUpdate: PropTypes.func.isRequired,
  flyTypes: PropTypes.any,
  card: PropTypes.shape({}),
  creating: PropTypes.bool.isRequired,
}

CreateCard.defaultProps = {
  flyTypes: null,
  card: {},
}

