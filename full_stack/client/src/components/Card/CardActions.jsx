import React from 'react';
import PropTypes from 'prop-types';

const CardActions = ({
  onClickNext,
  onClickUpdate,
  onClickDelete,
  toggleCreate,
}) => {
  return (
    <div className="card-actions">
      <button onClick={toggleCreate}>New</button>
      <button onClick={onClickDelete}>Delete</button>
      <button onClick={onClickUpdate}>Edit</button>
      <button onClick={onClickNext}>Next</button>
    </div>
  );
};

export default CardActions;

CardActions.propTypes = {
  onClickNext: PropTypes.func.isRequired,
  onClickUpdate: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  toggleCreate: PropTypes.func.isRequired,
};
