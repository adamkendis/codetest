import React from 'react';
import PropTypes from 'prop-types';
import './Card.css'

const Card = ({
  card,
  children,
}) => {
  const { descr, factoid, imgUrl, type: { name } } = card;
  return (
    <div className="card">
      <div className="card-image">
        <img src={imgUrl}></img>
      </div>
      <div className="card-content">
        <h3>{descr}</h3>
        <h4>{name}</h4>
        <p>{factoid}</p>
      </div>
      {children}
    </div>
  );
};

export default Card;
export { default as CardActions} from './CardActions';

Card.propTypes = {
  card: PropTypes.shape({
    id: PropTypes.number,
    descr: PropTypes.string,
    factoid: PropTypes.string,
    imgUrl: PropTypes.string,
    typeId: PropTypes.number,
    type: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      descr: PropTypes.string,
    }),
  }).isRequired,
  children: PropTypes.node,
};
