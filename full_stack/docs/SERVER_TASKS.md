### Server (express)

##### Tasks
 - [x] Implement barebones express server
 - [X] Determine routes, allowed methods

 ```
  /api/flies
    GET - list all flies
    POST - create new fly
  
  /api/flies/:id
    GET - get fly by primary key
    PATCH - update fly by primary key
    DELETE - delete fly by primary key

  /api/flytypes
    GET - list all flyTypes
 ```
 
 - [X] Implement Fly controllers per allowed methods above
 - [X] Implement FlyTypes controller
 - [X] Implement routes with corresponding controllers
 - [ ] API documentation (Swagger)
