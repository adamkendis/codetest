### Initial Setup

- [x] App structure
- [x] eslint

### Database (pg, sequelize)

##### Tasks
 - [x] Determine resources, relationship(s)

 ```javascript
 Fly
 {
   id: INT auto-incr pk,
   descr: STRING non-null,
   fact: STRING non-null,
   imgUrl: STRING non-null,
   typeId: INT fk belongsTo FlyType,
 }
 ```

 ```javascript
 FlyType
 {
   id: INT auto-incr pk,
   name: STRING isIn ['Dry Fly', 'Wet Fly', 'Streamer'],
   descr: STRING
 }
 ```
 
 - [X] Create model(s)
 - [X] Create and run migration
 - [X] Create and run seeder
