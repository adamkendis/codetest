### Client (React)

##### Tasks
 - [x] Simple wireframes
 - [X] Components
   - Card
   - EditableCard
   - Button - submit, delete, swipe next, create new
   - simple Header
 - [x] Data fetching
 - [x] API service
    - fetch all flies
    - create fly
    - update fly
    - delete fly
    - fetch all fly types
  - [x] Styles
