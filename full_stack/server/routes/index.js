const router = require('express').Router();
const fliesRouter = require('./flies');
const flyTypesRouter = require('./flyTypes');

/**
 * Mount respective router on path:
 *   - /api/flies
 *   - /api/flytypes
 */
router.use('/flies', fliesRouter);
router.use('/flytypes', flyTypesRouter);

// Status check endpoint at /api/status
router.get('/status', (req, res) =>
  res.status(200).json({
    status: `OK at ${new Date()}`,
    uptime: process.uptime(),
  })
);

module.exports = router;
