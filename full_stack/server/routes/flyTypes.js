const router = require('express').Router();
const flyTypesController = require('../controllers/flyTypes');

router.route('/')
  .get(flyTypesController.list)

module.exports = router;
