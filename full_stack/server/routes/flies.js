const router = require('express').Router();
const flyController = require('../controllers/fly');

router.route('/')
  .get(flyController.list)
  .post(flyController.create)

  router.route('/:id')
  .get(flyController.getById)
  .patch(flyController.update)
  .delete(flyController.delete);

module.exports = router;
