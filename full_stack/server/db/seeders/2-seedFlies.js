'use strict';

const db = require('../models');

module.exports = {

  // Command to seed 'flies' table
  up: async (queryInterface, Sequelize) => {
    const typeIds = await db.FlyType.findAll({
      attributes: ['id'],
      raw: true,
    });
    // let demoFlies = [];

    // for (let i = 0; i < typeIds.length * 3; i++) {
    //   demoFlies.push({
    //     descr: 'Parachute Adams',
    //     factoid: 'The Parachute Adams is a legendary fish catcher due to its unique ability to imitate the numerous members of the mayfly and caddis families. If no hatches are present on the water the Parachute Adams can be equally successful as a searching pattern.',
    //     imgUrl: 'https://www.crosscurrents.com/wp-content/uploads/2019/02/Parachute-Adams-Dry-Fly.jpg',
    //     typeId: 1,
    //   });
    // }

    await queryInterface.bulkInsert('flies', seedFlies);
  },

  // Command to revert 'flies' seed
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('flies');
  }
};

const seedFlies = 
[
  {
    descr: 'Parachute Adams',
    factoid: 'The Parachute Adams is a legendary fish catcher due to its unique ability to imitate the numerous members of the mayfly and caddis families. If no hatches are present on the water the Parachute Adams can be equally successful as a searching pattern.',
    imgUrl: 'https://www.crosscurrents.com/wp-content/uploads/2019/02/Parachute-Adams-Dry-Fly.jpg',
    typeId: 1,
  },
  {
    descr: 'Royal Wulff',
    factoid: 'The Royal Wulff is a popular artificial fly used for dry fly fishing. It is an attractor pattern.',
    imgUrl: 'https://thecatchandthehatch.com/wp-content/uploads/product_images/royal-wulff-red-sku-129.jpg',
    typeId: 1,
  },
  {
    descr: 'Copper John',
    factoid: 'The Copper John is a heavily weighted dropper pattern to drag down a smaller fly fished in tandem, otherwise known as copper/dropper',
    imgUrl: 'https://theflycrate.com/wp-content/uploads/2016/12/Copper-John.png',
    typeId: 2,
  },
  {
    descr: 'Egg Fly',
    factoid: 'An egg fly, like the name states, is tied specifically to represent a fish egg, unlike many dry flies where they typically imitate an insect.',
    imgUrl: 'https://www.madriveroutfitters.com/images/product/large/glo-bug-egg-fly-pink-2.jpg',
    typeId: 2,
  },
  {
    descr: 'Wooly Bugger',
    factoid: 'The Woolly Bugger is an artificial fly commonly categorized as a wet fly or streamer and is fished under the water surface.',
    imgUrl: 'https://www.madriveroutfitters.com/images/product/large/thin-mint-wooly-bugger.jpg',
    typeId: 3,
  },
]
