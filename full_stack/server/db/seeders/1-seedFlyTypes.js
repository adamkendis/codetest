'use strict';

module.exports = {

  // Command to seed 'flyTypes' table
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('flyTypes', [
      {
        name: 'Dry Fly',
        descr: 'A dry fly imitates an insect floating on top of the water.',
      },
      {
        name: 'Wet Fly',
        descr: 'Wet flies are designed to be fished below the water\'s surface.',
      },
      {
        name: 'Streamer',
        descr: 'Streamers are larger flies that are fished subsurface with an active retrieve.',
      },
    ]);
  },

  // Command to revert 'flyTypes' seed
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('flyTypes');
  }
};
