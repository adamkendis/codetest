module.exports = {
  development: {
    username: 'flyboxuser',
    password: 'supersecure',
    database: 'flybox',
    host: process.env.DATABASE_URL,
    dialect: 'postgres',
  },
  production: {
    use_env_variable: "DATABASE_URL",
    dialect: "postgres",
    dialectOptions: {
      ssl: {
        rejectUnauthorized: false
      },
    },
  },
};
