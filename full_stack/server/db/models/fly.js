'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Fly extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.FlyType, {
        foreignKey: 'typeId',
        as: 'type',
      });
    }
  }

  Fly.init({
    descr: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    factoid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    imgUrl:{
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'Fly',
    tableName: 'flies',
    timestamps: false,
    defaultScope: {
      attributes: { exclude: ['FlyTypeId'] }
    }
  });

  return Fly;
};
