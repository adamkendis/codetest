'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class FlyType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Fly, {
        as: 'flies',
      });
    }
  }

  FlyType.init({
    name: DataTypes.STRING,
    descr: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'FlyType',
    tableName: 'flyTypes',
    timestamps: false,
  });
  return FlyType;
};
