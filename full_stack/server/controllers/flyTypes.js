const db = require('../db/models');

module.exports = {

  // Get all rows from FlyType model
  list: async (req, res) => {
    try {
      const data = await db.FlyType.findAll();
      return res.status(200).json(data);
    } catch(e) {
      return res.status(500).send({
        error: e.message,
      });
    }
  },

};
