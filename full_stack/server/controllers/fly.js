const db = require('../db/models');

// Controller functions for Fly model CRUDL operations

module.exports = {

  // Query all flies, eager loading associated flyType
  list: async (req, res) => {
    try {
      const data = await db.Fly.findAll({
        include: {
          model: db.FlyType,
          as: 'type',
        },
        order: [['id', 'DESC']],
      });
      return res.status(200).json(data);
    } catch(e) {
      res.status(500).json({
        error: e.message,
      });
    }
  },

  create: async (req, res) => {
    try {
      const data = await db.Fly.create(req.body, {
        include: {
          model: db.FlyType,
          as: 'type',
        },
      });
      res.status(201).json(data);
    } catch(e) {
      return res.status(500).json({
        error: e.message,
      });
    }
  },

  // Update fly and return updated record in response
  update: async (req, res) => {
    try {
      const { id } = req.params;
      const { descr, factoid, imgUrl, typeId } = req.body;
      const data = await db.Fly.update({
        descr,
        factoid,
        imgUrl,
        typeId,
      }, {
        where: { id },
        returning: true,
      });
      res.status(201).json(data[1][0]);
    } catch(e) {
      return res.status(500).json({
        error: e.message,
      });
    }
  },

  delete: async (req, res) => {
    try {
      const { id } = req.params;
      await db.Fly.destroy({
        where: { id },
      });
      res.status(200).json({
        status: 'success',
      });
    } catch(e) {
      return res.status(500).json({
        error: e.message,
      });
    }
  },

  getById: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await db.Fly.findByPk(id);
      return res.status(200).json(data);
    } catch(e) {
      res.status(500).json({
        error: e.message,
      });
    }
  },
};
