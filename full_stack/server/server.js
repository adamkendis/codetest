const express = require('express');
const cors = require('cors');
const path = require('path');
const router = require('./routes');
const PORT = process.env.PORT || 3000;

const app = express();

// Middleware
app.use(express.static(path.join(__dirname, '../client/dist')))
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// Mount the router on the app at /api path
app.use('/api', router);

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
